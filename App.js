/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import TapScreen from './src/screens/TabScreens';

export default class App extends Component {
  render() {
    return <TapScreen />;
  }
}
