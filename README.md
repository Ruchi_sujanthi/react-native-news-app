# React Native News App

This mobile app contain news and news can search from different categories. 

**Git clone and set up**

* git clone https://gitlab.com/Ruchi_sujanthi/react-native-news-app.git
* cd my-project
* npm install
* react-native run-android for andriod
* react-native run-ios for ios

**Install and run**

Pre requisites:
 
* Install: Node.js, react native using npm.
* Libraries used: moment, react-native, native-base.

APIs used:
 * News API - 'https://newsapi.org/'
 * A simple and easy-to-use API that returns JSON metadata for headlines and articles live all over the web right now. — NewsAPI.org


App contain three tabs:
 
* Top Headline news with image.
* Custom news based on user preferences.
* Profile

Note
 * User can register with username at profile and data (user preferences) will be saved on local storage.
 * User can serach news in custom news tab using serach box.
 * application can be built on Android / iOS.
 
