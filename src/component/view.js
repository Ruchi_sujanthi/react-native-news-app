/* eslint-disable no-undef */
import React, {Component} from 'react';
import {Modal, Dimensions, Share} from 'react-native';
import {WebView} from 'react-native-webview';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Icon,
  Content,
  Button,
} from 'native-base';
const webHeight = Dimensions.get('window').height - 56;
export default class ModalView extends Component {
  constructor(props) {
    super(props);
    this.date = props.time;
  }

  handleClose = () => {
    this.props.onClose();
  };

  handleShare = () => {
    const {url, title} = this.props.newsData;
    const message = `${title}\n\nRead more @${url}\n\n Shared via NewsApp`;
    return Share.share(
      {title, message, url: message},
      {dialogTitle: `Share ${title}`},
    );
  };

  render() {
    const {showModel, newsData} = this.props;
    const {url} = newsData;
    if (url !== 'undefined') {
      return (
        <Modal
          animationType="slide"
          transparent
          visible={showModel}
          onRequestClose={this.handleClose}>
          <Container style={{margin: 15, marginBottom: 0}}>
            <Header>
              <Left>
                <Button onPress={this.handleClose} transparent>
                  <Icon name="close" style={{fontSize: 12, color: 'white'}} />
                </Button>
              </Left>
              <Body>
                <Title children={newsData.title} style={{color: 'white'}} />
              </Body>
              <Right>
                <Button onPress={this.handleShare} transparent>
                  <Icon name="share" style={{fontSize: 12, color: 'white'}} />
                </Button>
              </Right>
            </Header>
            <Content contentContainerStyle={{height: webHeight}}>
              <WebView
                source={{uri: url}}
                style={{flex: 1}}
                onError={this.handleClose}
                startInLoadState
                scalesPageToFit
              />
            </Content>
          </Container>
        </Modal>
      );
    } else {
      return null;
    }
  }
}
