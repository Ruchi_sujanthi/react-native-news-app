import React, {Component} from 'react';
import {Header, Item, Input, Icon, Button, Text} from 'native-base';
import {Keyboard} from 'react-native';

export default class SearchBarExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: this.props.text,
    };
  }
  updateSearch = search => {
    this.setState({search});
  };

  hanldeSubmit = () => {
    Keyboard.dismiss();
    this.props.handleInput(this.state.search);
  };

  render() {
    const {search} = this.state;
    return (
      <Header searchBar rounded>
        <Item>
          <Icon name="ios-search" />
          <Input
            placeholder="Search news by category"
            onChangeText={this.updateSearch}
            value={search}
          />
          <Button transparent onPress={this.hanldeSubmit}>
            <Text>Search</Text>
          </Button>
        </Item>
      </Header>
    );
  }
}
