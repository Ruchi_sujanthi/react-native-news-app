/* eslint-disable react/no-did-mount-set-state */
import React, {Component} from 'react';
import {Form, Item, Input, Label, Button, Text} from 'native-base';
import {Keyboard} from 'react-native';

export default class FloatingLabelExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: this.props.username,
      preference: this.props.preference,
    };
  }

  componentDidMount() {
    this.setState({preference: this.props.preference});
  }

  hanldeSubmit = () => {
    Keyboard.dismiss();
    this.props.handleInput(this.state.username, this.state.preference);
  };

  updateUserName = username => {
    this.setState({username});
  };

  updatePreference = preference => {
    this.setState({preference});
  };

  render() {
    if (this.props.username !== '') {
      return (
        <Form>
          <Item inlineLabel disabled>
            <Label>Username</Label>
            <Input disabled value={this.props.username} />
          </Item>
          <Item inlineLabel last>
            <Label>Preference</Label>
            <Input disabled value={this.props.preference} />
          </Item>
        </Form>
      );
    } else {
      return (
        <Form>
          <Item inlineLabel>
            <Label>Username</Label>
            <Input
              placeholder="Please Enter Username"
              onChangeText={this.updateUserName}
              value={this.state.username}
            />
          </Item>
          <Item inlineLabel last>
            <Label>Preference</Label>
            <Input
              placeholder="Please Enter Prefrence"
              onChangeText={this.updatePreference}
              value={this.state.preference}
            />
          </Item>
          <Button
            onPress={this.hanldeSubmit}
            style={{marginLeft: 15, marginRight: 15, marginTop: 40, flex: 1}}>
            <Text>Signin</Text>
          </Button>
        </Form>
      );
    }
  }
}
