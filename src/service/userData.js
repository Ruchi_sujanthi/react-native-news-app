/* eslint-disable no-const-assign */
/* eslint-disable no-undef */
import {AsyncStorage} from 'react-native';

export async function storeData(username, prefernce = 'general') {
  try {
    await AsyncStorage.setItem('username', username);
    await AsyncStorage.setItem('prefernce', prefernce);
    let result = true;
    return result;
  } catch (error) {
    console.log('ruchini error', error);
    throw error;
  }
}

export async function retrieveData() {
  try {
    let result = {
      username: '',
      prefernce: '',
    };
    const username = await AsyncStorage.getItem('username');
    const prefernce = await AsyncStorage.getItem('prefernce');
    if (username !== null) {
      result.username = username;
    }
    if (prefernce !== null) {
      result.prefernce = prefernce;
    }
    return result;
  } catch (error) {
    console.log('ruchini error', error);
    throw error;
  }
}
