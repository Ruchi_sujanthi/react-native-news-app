import {news_url, country_code, api_key} from '../config/api_config';

export async function getTopNews(category = 'general') {
  try {
    let articles = await fetch(
      `${news_url}?country=${country_code}&category=${category}`,
      {
        headers: {
          'X-API-KEY': api_key,
        },
      },
    );

    let result = await articles.json();
    articles = null;
    return result.articles;
  } catch (error) {
    console.log('ruchini error', error);
    throw error;
  }
}
