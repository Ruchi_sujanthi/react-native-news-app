import React, {Component} from 'react';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Tab,
  Tabs,
} from 'native-base';
import TopNews from './tabs/topNews';
import CustomNews from './tabs/customeNews';
import Profile from './tabs/profile';

export default class MainTabs extends Component {
  render() {
    return (
      <Container>
        <Header hasTabs>
          <Left />
          <Body>
            <Title>News App</Title>
          </Body>
          <Right />
        </Header>
        <Tabs>
          <Tab heading="Top News">
            <TopNews />
          </Tab>
          <Tab heading="Custom News">
            <CustomNews />
          </Tab>
          <Tab heading="Profile">
            <Profile />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
