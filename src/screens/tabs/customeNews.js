import React, {Component} from 'react';
import {Container, Content, List, Body} from 'native-base';
import {Alert, View, ActivityIndicator, Text} from 'react-native';
import {getTopNews} from '../../service/news';
import DataItem from '../../component/dataItem';
import Modal from '../../component/view';
import SearchBar from '../../component/searchBar';
import {retrieveData} from '../../service/userData';

export default class CustomeNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      data: null,
      setModeVisibility: false,
      viewNewsData: {},
      serachText: '',
    };

    this.handleInputValue = this.handleInputValue.bind(this);
  }

  handleModelOpen = topNews => {
    this.setState({
      setModeVisibility: true,
      viewNewsData: topNews,
    });
  };

  handleModelClose = () => {
    this.setState({
      setModeVisibility: false,
      viewNewsData: {},
    });
  };

  async handleInputValue(text) {
    this.setState({
      isLoading: true,
      serachText: text,
    });
    await getTopNews(text).then(
      data => {
        this.setState({
          isLoading: false,
          data: data,
          serachText: '',
        });
      },
      error => {
        Alert.alert(
          'Error',
          'Opps! Something went wrong. Please try again later.',
        );
      },
    );
  }

  async componentDidMount() {
    await retrieveData().then(data => {
      if (data.prefernce !== '') {
        this.setState({
          preference: data.prefernce,
        });
      }
    });
    getTopNews(this.state.preference).then(
      data => {
        this.setState({
          isLoading: false,
          data: data,
          serachText: this.state.preference,
        });
      },
      error => {
        Alert.alert(
          'Error',
          'Opps! Something went wrong. Please try again later.',
        );
      },
    );
  }
  render() {
    let view = this.state.isLoading ? (
      <View>
        <ActivityIndicator animating={true} />
        <Text>Please wait..</Text>
      </View>
    ) : (
      <List
        dataArray={this.state.data}
        renderRow={item => {
          return <DataItem onPress={this.handleModelOpen} data={item} />;
        }}
      />
    );
    return (
      <Container>
        <SearchBar
          handleInput={this.handleInputValue}
          text={this.state.serachText}
        />
        <Content>{view}</Content>
        <Modal
          showModel={this.state.setModeVisibility}
          newsData={this.state.viewNewsData}
          onClose={this.handleModelClose}
        />
      </Container>
    );
  }
}
