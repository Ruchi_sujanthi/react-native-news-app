import React, {Component} from 'react';
import {Container, Content, List} from 'native-base';
import {Alert, View, ActivityIndicator, Text} from 'react-native';
import {getTopNews} from '../../service/news';
import DataItem from '../../component/dataItem';
import Modal from '../../component/view';

export default class TopNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      data: null,
      setModeVisibility: false,
      viewNewsData: {},
    };
  }

  handleModelOpen = topNews => {
    this.setState({
      setModeVisibility: true,
      viewNewsData: topNews,
    });
  };

  handleModelClose = () => {
    this.setState({
      setModeVisibility: false,
      viewNewsData: {},
    });
  };

  componentDidMount() {
    getTopNews().then(
      data => {
        this.setState({
          isLoading: false,
          data: data,
        });
      },
      error => {
        Alert.alert(
          'Error',
          'Opps! Something went wrong. Please try again later.',
        );
      },
    );
  }
  render() {
    let view = this.state.isLoading ? (
      <View>
        <ActivityIndicator animating={true} />
        <Text>Please wait..</Text>
      </View>
    ) : (
      <List
        dataArray={this.state.data}
        renderRow={item => {
          return <DataItem onPress={this.handleModelOpen} data={item} />;
        }}
      />
    );
    return (
      <Container>
        <Content>{view}</Content>
        <Modal
          showModel={this.state.setModeVisibility}
          newsData={this.state.viewNewsData}
          onClose={this.handleModelClose}
        />
      </Container>
    );
  }
}
